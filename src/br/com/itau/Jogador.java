package br.com.itau;

import java.util.ArrayList;

public class Jogador {
    private String nome;
    private int pontos;
    private ArrayList<Jogador> jogador = new ArrayList<>();

    public Jogador(String nome, int pontos) {
        this.nome = nome;
        this.pontos = pontos;
    }

    public Jogador() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public ArrayList<Jogador> getJogador() {
        return jogador;
    }

    public boolean existeCadastro(Jogador jogador){
      boolean isCadastrado = false;
        for (Jogador jogadorRecorde: this.jogador) {
            if(jogadorRecorde.getNome().equalsIgnoreCase(jogador.getNome())){
                isCadastrado = true;
            }
        }

        return isCadastrado;
    }

    public void addJogador(Jogador jogadorRecorde){
        if(existeCadastro(jogadorRecorde)) {
            jogador.add(jogadorRecorde);
        }else
        {
            alterarPontuacao(jogadorRecorde);
        }
    }

    public void alterarPontuacao(Jogador jogador){
        for (Jogador jogadorRecorde: this.jogador) {
            if(jogadorRecorde.getNome().equalsIgnoreCase(jogador.getNome()) && jogadorRecorde.getPontos() < jogador.getPontos()){
                jogadorRecorde.setPontos(jogador.getPontos());
            }
        }
    }
}
