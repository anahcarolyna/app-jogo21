package br.com.itau;

public class Carta {
    public final  static int copas = 0;
    public final  static int espadas = 1;
    public final  static int ouro = 2;
    public final  static int paus = 3;

    private int naipe;
    private int tipo;

    public Carta(int naipe, int tipo) {
        this.naipe = naipe;
        this.tipo = tipo;
    }

    public int getNaipe() {
        return naipe;
    }

    public void setNaipe(int naipe) {
        this.naipe = naipe;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String imprimirNaipe(){
        switch(naipe){
            case 0: return "Copas";
            case 1: return  "Espada";
            case 2: return "Ouro";
            case 3: return "Paus";
            default:return "Não existe";
        }
    }
    public String imprimirCarta(){
        String retorno = null;
        switch(tipo){
            case 1: retorno = "as"; break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10: retorno = this.getTipo() +""; break;
            case 11: retorno = "valete"; break;
            case 12: retorno = "dama"; break;
            case 13: retorno = "rei"; break;
        }

        switch(naipe){
            case 3: return retorno + " de paus";
            case 2: return retorno + " de ouros";
            case 1: return retorno + " de espadas";
            case 0: return retorno + " de copas";
        }

        return retorno;
    }
}
