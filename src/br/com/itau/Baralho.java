package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Baralho {
    private List<Carta> baralho21;

    private int topo = 0;

    public Baralho() {
        this.baralho21 = new ArrayList<>(52);

        for (int naipe = 0;naipe <= 3; naipe++){
            for(int tipo = 1; tipo < 14; tipo++){
                Carta carta = new Carta(naipe, tipo);
                baralho21.add(carta);
                topo++;
            }
        }
    }


    public void embaralhar(){
        for(int ultimo= baralho21.size() -1; ultimo > 0; ultimo--){
            int posicaoAleatoria = (int)(Math.random() * (ultimo + 1));

            Carta cartaSelecionada = baralho21.get(posicaoAleatoria);
            baralho21.set(posicaoAleatoria, baralho21.get(ultimo)) ;
            baralho21.set(ultimo,cartaSelecionada);

        }
    }

    public Carta proximaCarta(){
        Carta carta = null;
        if(topo > 0){
            carta = baralho21.get(--topo);
            baralho21.remove(carta);
        }
        return carta;
    }
}
