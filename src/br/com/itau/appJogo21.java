package br.com.itau;

import javax.swing.*;

public class appJogo21 {
    public static void main(String[] args) {
       int numeroRodadas = Integer.parseInt(JOptionPane.showInputDialog(null, "Seleciona o número de rodadas: "));

       for(int contador=1; contador <= numeroRodadas;contador++) {
           Jogo.Jogar();
       }
    }
}
