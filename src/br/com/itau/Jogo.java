package br.com.itau;

import javax.swing.*;

public class Jogo {

    public static int somaCartas(Carta mao[]){
        int i = 0;
        int pontos = 0;

        while(mao[i] != null){
            if(mao[i].getTipo() > 10)
                pontos += 10;
            else
                pontos += mao[i].getTipo();
            i++;
        }
        return pontos;
    }

    public static void mostraCartas(Carta mao[], boolean isComputador) {
        int contador = 0;
        String cartasRodada = "";
        while (mao[contador] != null) {
            cartasRodada += mao[contador].imprimirCarta() + "\n";
            contador++;
        }
        if (!isComputador) {
            JOptionPane.showMessageDialog(null, "Suas cartas:\n" + cartasRodada +
                    "\nPontos = " + somaCartas(mao));
        } else {
            JOptionPane.showMessageDialog(null, "Computador cartas:\n" + cartasRodada +
                    "\nPontos = " + somaCartas(mao));
        }
    }

    public static void Jogar(){
        Jogador jogador = new Jogador(JOptionPane.showInputDialog(null, "Digite seu nome:", "Jogador 1:", JOptionPane.INFORMATION_MESSAGE),0);
        Jogador jogador2 = new Jogador("Computador", 0);

        Carta cartasJogador1[] = new Carta[19];
        Carta cartasJogador2[] = new Carta[19];

        Baralho baralho = new Baralho();
        baralho.embaralhar();

        String finalizaJogada = "N";
        int jogada = 0;
        do{
            cartasJogador1[jogada] = baralho.proximaCarta();
            Jogo.mostraCartas(cartasJogador1, false);
            jogador.setPontos(Jogo.somaCartas(cartasJogador1));

            if(jogador.getPontos() < 21){
                finalizaJogada = JOptionPane.showInputDialog(null, "\"Quer carta? S ou N\"", "Jogador" + jogador.getNome(), JOptionPane.INFORMATION_MESSAGE);

            }
            jogada++;

        }while (finalizaJogada.equalsIgnoreCase("S") && jogador.getPontos() <= 21);

        if(jogador.getPontos()  > 21){
            JOptionPane.showMessageDialog(null, "Voce perdeu!");
        }
        else {
            jogador.addJogador(jogador);

            int jogadaComp = 0;
            while (jogador2.getPontos() < jogador.getPontos() && jogador2.getPontos() != 21) {
                cartasJogador2[jogadaComp] = baralho.proximaCarta();
                jogador2.setPontos(somaCartas(cartasJogador2));
                jogadaComp++;
            }
            mostraCartas(cartasJogador2,true);

            if (jogador2.getPontos() == jogador.getPontos()) {
                JOptionPane.showMessageDialog(null, "Empate!");
            } else if (jogador2.getPontos() >= jogador.getPontos() && jogador2.getPontos() <= 21) {
                JOptionPane.showMessageDialog(null, "Voce perdeu pro Computador!");
            } else {
                JOptionPane.showMessageDialog(null, "Voce ganhou " + jogador.getNome() + "\n Recorde atual: " +
                        jogador.getPontos() + " pontos");
            }
        }
    }
}
